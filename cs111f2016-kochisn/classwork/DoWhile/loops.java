// Noah Kochis
// 11 07 16
public class loops
{
        public static void main (String args[])
        {
            // while loop
            int count = 0;
            while(count <100)
            {
                System.out.println(count);
                count+=10;
            }

            //do while
            count = 0;
            do
            {
                System.out.println(count);
                count+=10;
            }
            while(count <100);

            //for loop
            for(count=0; count <100; count+=10);
                System.out.println(count);
        }
}
