// Noah Kochis
// 11 02 2016

import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;
import java.io.IOException;

public class Main
{
    public static void main (String args[]) throws IOException
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("What would you like to search for?");
        String keyword = scan.next();

        fileReader reader = new fileReader();

        reader.readFile(keyword);
        System.out.println(reader.getList());
        System.out.println(keyword+" occured "+reader.getList().size()+" times.");
    }
}
