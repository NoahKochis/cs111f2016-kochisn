// Noah Kochis
// 11 02 16

import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;
import java.io.FileNotFoundException;

public class fileReader
{
    // instance variables
    private String fileName;
    private Scanner scanner;
    private ArrayList<String> list;

    //constructor
    public fileReader() throws FileNotFoundException
    {
        fileName="SacramentocrimeJanuary2006.csv";
        scanner = new Scanner(new File(fileName));
        list = new ArrayList<String>();
    }

    // set method
    public void readFile(String word)
    {
        scanner.useDelimiter(",");
        //read file
        while(scanner.hasNext())
        {
            String line = scanner.nextLine();
            if(line.contains(word))
            {
                list.add(line);
            }
        }
        scanner.close();
    }

    // get method
    public ArrayList<String> getList()
    {
        return list;
    }
}
