
//*************************************
// Honor Code:  This work is mine unless otherwise cited.
// Noah Kochis
// CMPSC 111 Fall 2016
// String Examples
// Date:  09 26 2016
//
// Purpose:  To use Strings
//*************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class bored
{
	//------------------------
	// main method:  program execution begins here
	//------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Noah Kochis\n String Examples\n" + new Date() + "\n");
        int length = 0;
        Scanner userInput = new Scanner(System.in);
        char c;
        int index;

        String word = " "; // creates empty Strings

        System.out.println("Enter a String");
        word = userInput.nextLine();

        System.out.println(word);

        length = word.length();
        System.out.println("Length:  " + length);

        c = word.charAt(3);
        System.out.println("4th charcter is:  " + c);

        index = word.indexOf('i');
        System.out.println("Index:  " + index);
	}
}
