//Now we have a .java file that uses the GradeBook class,
//like creating an int variable. This creates or
//instantiates a new variable or object
//called myGradeBook of the GradeBook class.
public class GradeBookTest
{
    public static void main ( String args[] )
    {
        GradeBook myGradeBook = new GradeBook ();
        //myGradeBook.displayMessage();
        myGradeBook.getCourseName();
    }
}
