//Noah Kochis
//09 14 16
import java.util.Scanner;

public class age
{
	public static void main (String[] args)
	{
		double age; // declaration
		age = 20; // assignment

		double result = 0; // declaration and assignment

		// declare Scanner object
		Scanner userInput = new Scanner(System.in);

		// prompt user for input
		System.out.println("Enter an age:  ");
		age = userInput.nextDouble();
		
		System.out.println("You entered:  "+age);

		// covert age to minutes
		result = age*365*24*60;
		System.out.println("Age in mintues:  "+result);
	}
}
