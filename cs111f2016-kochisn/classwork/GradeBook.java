// Define class GradeBook with a method displayMessage
public class GradeBook
{

    String courseName = "CMPSC 111";

    // method to display a welcome message
    // to the GradeBook user
    public void displayMessage (String courseName)
    {
        System.out.println("Welcome to the Grade Book for "+courseName);
    }

    public String getCourseName()
    {
        return courseName;

    }
}
