// It asks users to input numbers, then the program arranges said numbers from lowest to highest.
import java.util.Scanner;

public class Order
{
    public static void main (String args[])
    {
        Scanner input = new Scanner(System.in);
        int x, y, z;

        System.out.println("Enter three numbers: ");
        x = input.nextInt();
        y = input.nextInt();
        z = input.nextInt();
        System.out.println("You entered: "+x+" , "+y+" , "+z);

        if(x<y && x<z && y<z)
        {
            System.out.println(x+" , "+y+" , "+z);
        }
        else if(x<y && x<z && y>z)
        {
            System.out.println(x+" , "+z+" , "+y);
        }
        else if(y<x && y<z && x<z)
        {
            System.out.println(y+" , "+x+" , "+z);
        }
        else if(y<x && y<z && x>z)
        {
            System.out.println(y+" , "+z+" , "+x);
        }
        else if(z<x && z<y && x<y)
        {
            System.out.println(z+" , "+x+" , "+y);
        }
        else if(z<x && z<y && x>y)
        {
            System.out.println(z+" , "+y+" , "+x);
        }
    }
}
