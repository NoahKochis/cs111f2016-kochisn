
//*************************************
// Honor Code:  This work is mine unless otherwise cited.
// Noah Kochis
// CMPSC 111 Fall 2016
// Classwork
// Date:  09 19 2016
//
// Purpose:  Use Scanners.
//*************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; // import scanner

public class Addition
{
	//------------------------
	// main method:  program execution begins here
	//------------------------
	public static void main(String args [])
	{
		String name = "";
		double first = 0;
		double second = 0;
		double sum = 0;

		Scanner input = new Scanner (System.in);

		System.out.println("Enter your full name");
		name = input.nextLine();
		System.out.println("You entered "+name);
		
		System.out.println("Enter a first number");
		first = input.nextDouble();

		System.out.println("Enter a second number");
		second = input.nextDouble();

		sum = first+second;
		System.out.println("Sum is "+sum);
		
	}
}
