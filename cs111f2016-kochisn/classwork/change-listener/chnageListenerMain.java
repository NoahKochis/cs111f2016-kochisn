import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;

public class chnageListenerMain
{
    public static void main (String args [])
    {
        // create progress bar
        JProgressBar myBar = new JProgressBar();
        myBar.setValue(50);

        // create a slider
        JSlider mySlider = new JSlider();

        mySlider.addChangeListener(new MyListener(myBar));

        JButton myButton = new JButton("I am pointless");

        JFrame frame = new JFrame("Change Listener");
        frame.setLayout(new FlowLayout());
        frame.add(myBar);
        frame.add(mySlider);
        frame.add(myButton);
        frame.pack();
        frame.setVisible(true);
    }

    public static class MyListener implements ChangeListener
    {
        // instance variable
        private JProgressBar myProgressBar;
        // constructor
        public MyListener (JProgressBar bar)
        {
            myProgressBar = bar;
        }

        public void stateChanged(ChangeEvent event)
        {
            JSlider mySlider = (JSlider)event.getSource();
            // change the value of the progress bar
            myProgressBar.setValue(mySlider.getValue());
        }
    }
}
