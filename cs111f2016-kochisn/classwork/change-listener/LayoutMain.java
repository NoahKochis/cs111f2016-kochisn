import java.awt.*;
import javax.swing.*;

public class LayoutMain
{
    public static void main (String args [])
    {
        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout());

        frame.add(new JButton("Butt 1"));
        frame.add(new JLabel("Lab 1"));
        frame.add(new JTextField("Text 1"));

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(0,1));

        panel.add(new JButton("Butt 2"));
        panel.add(new JLabel("Lab 2"));
        panel.add(new JTextField("Text 2"));

        frame.add(panel);

        frame.pack();
        frame.setVisible(true);
    }
}
