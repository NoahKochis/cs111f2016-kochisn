//=================================================
// Class Example using Graphics
// November 21, 2016
// Janyl Jumadinova
//
// Purpose: Illustrate Graphics class methods
//=================================================
import javax.swing.*;import java.awt.*;public class BasicGraphics extends javax.swing.JApplet  {	public void paint(Graphics g) {        	/* Draw the square. */          	g.setColor(Color.blue);          	g.fillRect(10, 20, 40, 40);        }  // end paint()

	public static void main(String[] args)
    	{
        	JFrame window = new JFrame("Janyl Jumadinova ");

      		// Add the drawing canvas and do necessary things to
     		// make the window appear on the screen!
        	window.getContentPane().add(new BasicGraphics());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
		window.setSize(600, 400);

        	//window.pack();
    	}
} // end class BasicGraphics
