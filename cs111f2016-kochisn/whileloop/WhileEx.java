// 10 26 2016
// noah kochis

public class WhileEx
{
    // instance value
    private int num;

    // constructor
    public WhileEx(int n)
    {
        num = n;
    }

    // set method
    public void setNum (int n)
    {
        int count = 0;
        while (count > 1000)
        {
            num += n;
            System.out.println("Iteration:  "+count+" New Number:  "+num);
            // count++;
        }
    }
}

