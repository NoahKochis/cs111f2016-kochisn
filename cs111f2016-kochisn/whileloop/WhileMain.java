
//*************************************
// Honor Code:  This work is mine unless otherwise cited.
// Noah Kochis
// CMPSC 111 Fall 2016
// class work
// Date:  10 26 2016
//
// Purpose:  Use loops.
//*************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; // scanner

public class WhileMain
{
	//------------------------
	// main method:  program execution begins here
	//------------------------
	public static void main(String args[])
	{
		// Label output with name and date:
		System.out.println("Noah Kochis\n" + new Date() + "\n");

        Scanner scan = new Scanner (System.in);

        System.out.println("Enter a positive integer");
        int count = 1;
        int number = 0;

        number = scan.nextInt();

        // validate user's input to ensure we get a positive number
        while(number < 0)
        {
            System.out.println("You need to enter a positive integer");
            number = scan.nextInt();
            System.out.println("Iteration:  "+count+" User's input:  "+number);
            count++;
        }

        // create instance of the WhileEx
        WhileEx numIterate = new WhileEx(0);
        numIterate.setNum(number);
    }
}
