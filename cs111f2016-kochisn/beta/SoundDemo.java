import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


/** Demonstrates how to play sounds using
  * the Applet AudioClip class.  Note that java.applet.*
  * must be imported to have the ability to use this class.
  * Also note the import of java.net.* to get the URL
  * class, because an URL is needed to make an AudioClip.
  *
  * <p>
  * You need wassup.wav and enjoysil.mid (or replace them with
  * files of your choice!) to run this program.
  *
  * @author Martin Stepp
  * @version November 12, 2000
  */
public class SoundDemo extends JFrame implements ActionListener {
	private AudioClip myWAVClip  = null;
	private AudioClip myMIDIClip = null;


	/** Runs the sound demo. */
	public static void main(String[] args) {
		SoundDemo demo = new SoundDemo();
		demo.show();
	}


	/** Constructs a new sound demo frame, sets up event listening,
	  * and lays out components.
	  */
	public SoundDemo() {
		super("Marty's Sound Demo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		myWAVClip  = getAudioClip("wassup.wav");
		myMIDIClip = getAudioClip("enjoysil.mid");

		JButton dingButton = new JButton("Play WAV Sound");
		JButton musicButton = new JButton("Play MIDI Music");
		JButton stopButton = new JButton("Stop");
		dingButton.addActionListener(this);
		musicButton.addActionListener(this);
		stopButton.addActionListener(this);

		// lay out the frame and its panels
		JPanel contentPane = new JPanel();
		contentPane.add(dingButton);
		contentPane.add(musicButton);
		contentPane.add(stopButton);

		setContentPane(contentPane);
		pack();  // resize window until it's just right
	}


	/** Responds to button clicks in this program.
	  *
	  * An ActionEvent's "action command" is a String associated with the event;
	  * in the case of an event triggered by a button, the action command is
	  * the text written on the button (unless you call setActionCommand on the
	  * button to change it to something else, which we didn't).  We'll use this
	  * to figure out which button the user pressed.
	  */
	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();

		if (command.equals("Play WAV Sound"))
			myWAVClip.play();

		else if (command.equals("Play MIDI Music"))
			myMIDIClip.loop();

		else if (command.equals("Stop")) {
			myWAVClip.stop();
			myMIDIClip.stop();
		}
	}


	/** Generates and returns a new AudioClip object for the given file name.
	  *
	  * AudioClips must be built using an URL,
	  * so we'll make one using the file name.
	  *
	  * A valid URL for the file wassup.wav in the C:\java folder would be
	  * file:C:\java\wassup.wav
	  *
	  * or, as a Java String,
	  * "file:C:\\java\\wassup.wav"
	  *
	  * System.getProperty("user.dir") returns a String
	  * representation of the current directory.
	*/
	public static AudioClip getAudioClip(String fileName) {
		URL address = null;
		try {
			address = new URL("file:" + System.getProperty("user.dir") + "\\" + fileName);
		} catch (MalformedURLException mfurle) {
			System.err.println("Couldn't make URL: " + mfurle);
		}

		return Applet.newAudioClip(address);
	}
}
