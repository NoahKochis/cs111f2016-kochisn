
//*************************************
// Honor Code:  This work is mine unless otherwise cited.
// Noah Kochis
// CMPSC 111 Fall 2016
// Midterm
// Date:  10 20 2016
//
// Purpose:  To tell a story.
//*************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class Midterm
{
	//------------------------
	// main method:  program execution begins here
	//------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Noah Kochis\n Mid term\n" + new Date() + "\n");

		String name;

		Scanner userInput = new Scanner(System.in);

		System.out.println("Type an adjective:");
		name = userInput.next();

        System.out.println(name+" day");
	}
}
