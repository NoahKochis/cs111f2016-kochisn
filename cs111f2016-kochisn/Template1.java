//*************************************
// Honor Coed:  This work is mine unless otherwise cited.
// Noah Kochis
// CMPSC 111 Fall 2016
// Lab 2
// Date:  09 08 2016
//
// Purpose:  To create a program template.
//*************************************
import java.util.Date; // needed for printing today's date

public class Template1
{
	//------------------------
	// main method:  program execution begins here
	//------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Noah Kochis\n Lab 2\n" + new Date() + "\n");

         int x[] = new int[10];
        for (int i = 0; i < x.length; i++)
        {
            x[i] = i+1;
        }
        for (int i = 0; i < x.length; i++)
        {
            if((x[i]%2!=0) || (i%2==0))
           {
                System.out.print(x[i] + ", ");
           }
        }
        System.out.println();
	}
}
