import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.awt.*;

import javax.swing.*;

import java.applet.Applet;
import java.applet.AudioClip;

public class drumMachineInterface {

    JButton Snare = new JButton("snare");
	JButton Kick = new JButton("kick");
    JButton hhClosed = new JButton("HH Closed");
    JButton hhOpen = new JButton("HH Open");
    JButton Clap = new JButton("Clap");
    JButton cowBell = new JButton("Cow Bell");
    JButton Cymbal = new JButton("Cymbal");
    JButton Tom = new JButton("Tom");
    JButton Tone = new JButton("Tone");

    JPanel panel = new JPanel();

    public drumMachineInterface() {
        JFrame frame = new JFrame("DRUM MACHINE");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        panel.add(Snare);
		panel.add(Kick);
        panel.add(hhClosed);
        panel.add(hhOpen);
        panel.add(Clap);
        panel.add(cowBell);
        panel.add(Cymbal);
        panel.add(Tom);
        panel.add(Tone);
        frame.pack();
        frame.add(panel);
        frame.setResizable(true);
        frame.setVisible(true);
		frame.setSize(200,300);

        Snare.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
				URL url = getClass().getClassLoader( ).getResource("Snare7.wav");
				AudioClip ac = Applet.newAudioClip(url);
				ac.play();
            }
        });

		Kick.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
				URL url = getClass().getClassLoader( ).getResource("Kick05.wav");
				AudioClip ac = Applet.newAudioClip(url);
				ac.play();
            }
        });


		Clap.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
				URL url = getClass().getClassLoader( ).getResource("Clap005.wav");
				AudioClip ac = Applet.newAudioClip(url);
				ac.play();
            }
        });

		hhClosed.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
				URL url = getClass().getClassLoader( ).getResource("HiHatClosed001Widened-24b.wav");
				AudioClip ac = Applet.newAudioClip(url);
				ac.play();
            }
        });

		hhOpen.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
				URL url = getClass().getClassLoader( ).getResource("HiHatOpen003.wav");
				AudioClip ac = Applet.newAudioClip(url);
				ac.play();
            }
        });

		cowBell.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
				URL url = getClass().getClassLoader( ).getResource("Bottle.wav");
				AudioClip ac = Applet.newAudioClip(url);
				ac.play();
            }
        });

		Cymbal.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
				URL url = getClass().getClassLoader( ).getResource("Crash-Cymbal-2.wav");
				AudioClip ac = Applet.newAudioClip(url);
				ac.play();
            }
        });

		Tom.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
				URL url = getClass().getClassLoader( ).getResource("IAMMDEEPHOUSE_Perc_16.wav");
				AudioClip ac = Applet.newAudioClip(url);
				ac.play();
            }
        });

		Tone.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
				URL url = getClass().getClassLoader( ).getResource("Vocal2.wav");
				AudioClip ac = Applet.newAudioClip(url);
				ac.play();
            }
        });

    }

    public static void main(String[] args)
    {
        new drumMachineInterface();
    }
}
