import java.awt.*;
import javax.swing.*;

public class drumMachineMain
{
    public static void main (String args [])
    {
        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout());

        // set up left-most panel
        JPanel panelA = new JPanel();
        panelA.setLayout(new GridLayout(0,1));

        // declare buttons for panelA
        JButton kick = new JButton("Kick");
        JButton snare = new JButton("Snare");
        JButton hhClosed = new JButton("HH Closed");

        // set up middle panel
        JPanel panelB = new JPanel();
        panelB.setLayout(new GridLayout(0,1));

        // declare buttons for panelB
        JButton hhOpen = new JButton("HH Open");
        JButton cymbal = new JButton("Cymbal");
        JButton cowBell = new JButton("Cow Bell");

        // set up right-most panel
        JPanel panelC = new JPanel();
        panelC.setLayout(new GridLayout(0,1));

        // declare buttons for panelC
        JButton clap = new JButton("Clap");
        JButton tom = new JButton("Tom");
        JButton why = new JButton("Helicopter");


        // add buttons to panelA
        panelA.add(hhClosed);
        panelA.add(snare);
        panelA.add(kick);

        // add buttons to panelB
        panelB.add(hhOpen);
        panelB.add(cymbal);
        panelB.add(cowBell);

        // add buttons panelC
        panelC.add(clap);
        panelC.add(tom);
        panelC.add(why);

        // add panels to frame
        frame.add(panelA);
        frame.add(panelB);
        frame.add(panelC);

        // close operation
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // make visible
        frame.pack();
        frame.setVisible(true);
    }
}
