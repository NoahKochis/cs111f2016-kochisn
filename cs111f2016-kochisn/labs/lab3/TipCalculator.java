
//*************************************
// Honor Code:  This work is mine unless otherwise cited.
// Noah Kochis
// CMPSC 111 Fall 2016
// Lab 3
// Date:  09 15 2016
//
// Purpose:  To create a program template.
//*************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; // imports scanner

public class TipCalculator
{
	//------------------------
	// main method:  program execution begins here
	//------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Noah Kochis\n Lab 3\n" + new Date() + "\n");

		String name; // declaration and assignment

		Scanner userInput = new Scanner(System.in); // declare Scanner object

		System.out.println("Could you tell me your name, please?"); // prompt for input
		name = userInput.next();

		System.out.println("Hello! I'm so glad you decided to use me as your tip calculator, "+name+"."); // frienldy message

		double bill;
		double result = 0;
		
		System.out.println("How much is your bill?");
		bill = userInput.nextDouble();

		double percentage;
		
		System.out.println("What percentage would you like to tip?");
		percentage = userInput.nextDouble();
		
		double tip;

		tip = percentage/100*bill;
		
		double total_bill = bill+tip;
		
		System.out.println("Your initial bill was $"+bill);
		System.out.println("Your tip amount is $"+tip); 
		System.out.println("Your total bill is now $"+total_bill);

		double numPeople;

		System.out.println("How many people are splitting the bill with?");
		numPeople = userInput.nextDouble();
		
		double share = total_bill/numPeople;

		System.out.println("Each person should pay $"+share);
		System.out.println("Glad I could be of service. Stay marvelous!");

	}
}
