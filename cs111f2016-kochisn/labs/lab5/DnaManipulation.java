
//***************
//Honor Code: This work is mine unless otherwise cited.
//Chris Taylor, Noah Kochis, Sarah Seitanakis
//CMPSC 111 Fall 2016
//Lab 5
//Date: 09-29-2016
//
//Purpose: To "manipulate DNA" by making a random replacement program.
//***************
import java.util.Date;
import java.util.Scanner;
import java.util.Random;

public class DnaManipulation
{
	//main method: program execution begins here
	public static void main(String[] args)
	{
        //Variable declarations
        String dna;
        String complement;
        String mutation;
        int length; // length of given String
        int location; // location of characters within given String
        char character; //character located with location

        //Non-variable variable declarations
        String ACTG = "ACTG";

        //Process declarations
        Scanner userInput = new Scanner(System.in);
        Random random = new Random();


		//Label output with name and date:
		System.out.println("Chris Taylor, Noah Kochis, Sarah Seitanakis\n Lab 5\n" + new Date() + "\n");

        //Requesting string
        System.out.println("Please enter a string consisting only of A, C, T, and G."); // prompt user for String
        dna = userInput.next(); //giving dna contents
        dna = dna.toUpperCase(); //making sure it's all upper-case
        System.out.println("You put in "+dna+"."); //repeating it to the user because they're stupid and can't remember what they typed

        //Conversion to complement

        complement = dna.replace('A','t'); //Changes A to T without making it change back when T is changed to A
        complement = complement.replace('T','a');
        complement = complement.replace('C','g'); //See previous comment
        complement = complement.replace('G','c');
        complement = complement.toUpperCase(); //Getting everything capitalized again
        System.out.println("The complement of "+dna+" is "+complement+"."); //Output

        //Randomizer stuff

        length = dna.length(); //Getting length for random insertions
        location = random.nextInt(length); //Picking a random spot in the chain
        character = ACTG.charAt(random.nextInt(4)); //Picking a random character from DNA's 4 options
        mutation = dna.substring(0,location)+character+dna.substring(location,length); //Adding a character in the middle of the input
        System.out.println("Inserting "+character+" at position "+(location+1)+" gives "+mutation+"."); //Printing out the string with the added character
        location = random.nextInt(length); //Choosing a new spot
        mutation = dna.substring(0,location)+dna.substring(location+1,length); //Removing a character
        System.out.println("Deleting from position "+(location+1)+" gives "+mutation+"."); //Printing out the string with the removed character
        location = random.nextInt(length); //Choosing a new spot
        character = ACTG.charAt(random.nextInt(4)); //See line 58
        mutation = dna.substring(0,location)+character+dna.substring(location+1,length); //Replacing a character
        System.out.println("Changing at position "+(location+1)+" gives "+mutation+"."); //Printing the replacement


	}
}
