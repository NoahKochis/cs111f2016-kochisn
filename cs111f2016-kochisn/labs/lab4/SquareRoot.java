
//*************************************
// Honor Code:  This work is mine unless otherwise cited.
// Noah Kochis
// CMPSC 111 Fall 2016
// Lab 2
// Date:  09 22 2016
//
// Purpose:  Calculate square root
//*************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class SquareRoot
{
	//------------------------
	// main method:  program execution begins here
	//------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Noah Kochis\n Lab 4\n" + new Date() + "\n");

        Scanner userInput = new Scanner(System.in);

        double value;
        double guess;

        System.out.println("Please insert a number");
        value = userInput.nextDouble();

        System.out.println("Please guess the square root of the number you just entered");
        guess = userInput.nextDouble();

        double first = .5*(guess*(value/guess));

        double second = .5*(first*(value/first));

        double third = first*second;

        System.out.println("Your guess was: " + guess + ". Our first answer was " + first + ". Our final answer was " + second + ". Guess * guess: " + third + ".");
    }
}
