
//*************************************
// Honor Code:  This work is mine unless otherwise cited.
// Noah Kochis
// CMPSC 111 Fall 2016
// Lab 2
// Date:  09 22 2016
//
// Purpose:  To count change
//*************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; // import scanner

public class Change
{
	//------------------------
	// main method:  program execution begins here
	//------------------------
	public static void main(String[] args)
	{
        Scanner userInput = new Scanner(System.in);

        int cents;

        int tens;
        int fives;
        int ones;
        int quarters;
        int dimes;
        int nickles;
        int pennies;

		// Label output with name and date:
		System.out.println("Noah Kochis\n Lab 4\n" + new Date() + "\n");

        System.out.println("How much, in cents, do we need to make change for?");
        cents = userInput.nextInt();

        System.out.println("to make change for " + cents + " cents, you need:");

        tens = cents/1000;
        cents = cents%1000;

        System.out.print(tens + " ten(s), ");

        fives = cents/500;
        cents = cents%500;

        System.out.print(fives + " five(s), ");

        ones = cents/100;
        cents = cents%100;

        System.out.print(ones + " one(s), ");

        quarters = cents/25;
        cents = cents%25;

        System.out.print(quarters + " quarter(s), ");

        dimes = cents/10;
        cents = cents%10;

        System.out.print(dimes + " dime(s), ");

        nickles = cents/5;
        cents = cents%5;

        System.out.print(nickles + " nickles(s), ");

        pennies = cents/1;

        System.out.print("and " + pennies + " penny(s).");
	}
}
