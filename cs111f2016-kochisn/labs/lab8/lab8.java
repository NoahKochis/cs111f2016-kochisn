//********************************************************
// Honor Coed: This work is mine unless otherwise cited.
// Noah Kochis
// CMPSC 111 Fall 2016
// Lab #: [x]
// Date: [x]
// Purpose: [x]
//*********************************************************
import java.util.Date;

public class lab8
{

	public static void main(String[] args)
	{
		//Label output with name and date:
		System.out.println("Noah Kochis\nLab#: 8\n" + new Date() + "\n");

        //prints top two rows
        for (int rows=0; rows < 2; rows++)
        {
            for (int num=0; num < 28; num++)
            {
                 System.out.print("*");
            }
            System.out.println();
	    }


        //prints checker patterns
        for (int x=0;x<10;x++)
        {
        for (int d=0; d<4; d++)
        {
            for (int b=0; b<4; b++)
            {
                System.out.print("*");
            }
            for (int c=0; c<4; c++)
            {
                System.out.print(" ");
            }
        }
        System.out.println();

        for (int d=0; d<3;d++)
        {
            for (int b=0; b<4; b++)
            {
                System.out.print(" ");
            }
            for (int c=0; c<4; c++)
            {
                System.out.print("*");
            }
        }
        System.out.println();
        }


        //prints bottom two rows
        for (int rows=0; rows < 2; rows++)
        {
            for (int num=0; num < 28; num++)
            {
                 System.out.print("*");
            }
            System.out.println();
	    }

    }
}
