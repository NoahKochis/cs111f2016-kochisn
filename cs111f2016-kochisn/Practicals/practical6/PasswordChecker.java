
//*************************************
// CMPSC 111 Fall 2016
// Practical 6
// Name:  Noah Kochis
// Purpose: Utilize if else statements to check the password validity
//*************************************

import java.util.Scanner;

public class PasswordChecker
{
	//-------------------------------
	// main method: program execution begins here
	//-------------------------------
	public static void main(String[] args)
	{

        //Declare and initialize variables
        Scanner scan = new Scanner(System.in);
        String input;
        String password;

        //Ask user if they want to pre-validate their password
        System.out.print("Would you like to pre-validate your new password? ");
        input = scan.next();

				// use nested if/else structure
        if(input.equalsIgnoreCase("yes"))
        {
          System.out.print("Please enter a password: ");
          password = scan.next();

					// check if the password contains any spaces
          if(password.contains(" "))
          {
            System.out.println("Fail: Your password contains a space!");
          }
          else
          {
            System.out.println("Pass: Your password does not contain a space!");
          }
                // check if password has at least 8 characters
          if(password.length()<8)
          {
          System.out.println("Fail: Your password has less than 8 characters!");
          }
          else
          {
          System.out.println("Pass: Your password has 8 or more characters!");
          }

            // check if password contains numerical value
          if(password.contains("1") || password.contains("2") || password.contains("3") || password.contains("4") || password.contains("5") || password.contains("6") || password.contains("7") || password.contains("8") || password.contains("9"))
          {
           System.out.println("Pass: Your password contains a numeric value!");
          }
          else
          {
           System.out.println("Fail: Your password does not contain a numeric value!");
          }
            // check if password contains dash
          if(password.contains("-") || password.contains("_"))
          {
           System.out.println("Pass:  Your password contains a dash!");
          }
          else
          {
           System.out.println("Fail: Your password does not contains a dash!");
          }

            //check for uppercase letter
          if(!password.equals(password.toLowerCase()))
          {
          System.out.println("Pass: Your password has one uppercase letter!");
          }
          else
          {
         System.out.println("Fail:Your passwords does not contain an uppercase letter!");
          }
            // check for lowercase letter
          if(!password.equals(password.toUpperCase()))
          {
          System.out.println("Pass: Your password contains a lowercase letter!");
          }
          else
          {
          System.out.println("Fail: Your password does not contain a lowercase letter!");
          }
              // TO DO: using similar if/else statements to the one above,
					//			  check for the other 5 password requirements

        }
				// if the user does not want to check the password, display
				// the requirements and save the user's password
        else
        {
          System.out.println("Password must meet these requirements:");
          System.out.println("   Must contain 8 characters.");
          System.out.println("   Must contain at least 1 numeric digit.");
          System.out.println("   Must contain at least 1 special character - or _.");
          System.out.println("   Must contain at least 1 upper case letter.");
          System.out.println("   Must contain at least 1 lower case letter. \n");
          System.out.print("Please enter a password: ");
          password = scan.next();

        }
	}
}
