
//*************************************
// Honor Code:  This work is mine unless otherwise cited.
// Noah Kochis
// CMPSC 111 Fall 2016
// Lab 2
// Date:  09 16 2016
//
// Purpose:  To tell a story.
//*************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class MadLibs
{
	//------------------------
	// main method:  program execution begins here
	//------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Noah Kochis\n Lab 2\n" + new Date() + "\n");
		
		String name;
		String noun;
		String adj;
		String adj2;
		int num;
		int num2;

		Scanner userInput = new Scanner(System.in);

		System.out.println("What's your name, patron?");
		name = userInput.next();

		System.out.println("Greetings, "+name+", I am writer for hire; a computer writer for hire! Please answer the following questions and I shall tell thee a story.");
		
		System.out.println("Could you give me a noun?");
		noun = userInput.next();

		System.out.println("How about an adjective?");
		adj = userInput.next();

		System.out.println("Another adjective?");
		adj2 = userInput.next();

		System.out.println("Now give me a non-zero, whole number, patron.");
		num = userInput.nextInt();
		
		System.out.println("Another whole number that isn't zero.");
		num2 = userInput.nextInt();

		double result = num2/num;

		System.out.println("Once upon a time, there was a "+adj+" king named Solomon. One day, during his rule, "+num+" "+adj2+" women came to him. They were fighting over  "+num2+" "+noun+"(s). Both claimed ownership of the "+noun+". Too resolves this, the king ordered that each "+noun+" be cut up into pieces. The "+noun+"(s) will be shared between the two women with each getting "+result+" equal pieces. One woman decides that this is a bad idea and Solomon dtermines that they are the true owener. And everyone lived happily ever after!");
	}
}
