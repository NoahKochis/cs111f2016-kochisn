// Noah Kochis
// 11 04 2016
import java.util.*;
import java.io.*;

public class Reader
{
    public static void main(String[] args) throws IOException
    {
        ArrayList<String> words = new ArrayList<String>();
        File file = new File("Ulysses.txt");
        Scanner in = new Scanner (file);
        int numWords = 0;
        while(in.hasNext())
        {
            String x = in.next().toLowerCase();
            numWords++;
        }
        System.out.println("Number of words: " + numWords);
    }
}
