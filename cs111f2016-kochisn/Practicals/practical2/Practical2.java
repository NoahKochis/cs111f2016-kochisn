
//*************************************
// Honor Coed:  This work is mine unless otherwise cited.
// Noah Kochis
// CMPSC 111 Fall 2016
// Practical 2
// Date:  09 09 2016
//
// Purpose:  To create a program template.
//*************************************
import java.util.Date; // needed for printing today's date

public class Practical2
{
	//------------------------
	// main method:  program execution begins here
	//------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Noah Kochis\n" + new Date() + "\n");
		System.out.println("\t 0");
		System.out.println("\t/|\\");
		System.out.println("\t |");
 		System.out.println("\t /\\");
		System.out.println("\t/  \\");
	}
}
