
//*************************************
// Honor Coed:  This work is mine unless otherwise cited.
// Noah Kochis
// CMPSC 111 Fall 2016
// Practical 5
// Date:  10 07 2016
//
// Purpose:  To create a program template.
//*************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class YearChecker
{
	//------------------------
	// main method:  program execution begins here
	//------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Noah Kochis\n Practical 5\n" + new Date() + "\n YearChecker");
	    Scanner userInput = new Scanner(System.in);
        int year;

        System.out.println("Enter a year between 1000 and 3000:  ");
        year = userInput.nextInt();

        if (year%100==0 && year%400==0)
            System.out.println(year+" is a leap year.");
        else if (year%100!=0 && year%4==0)
            System.out.println(year+" is a leap year.");
        else
            System.out.println(year+" is not a leap year");

        if ((year-2013)%17==0)
            System.out.println("It's an emergence year for Brood II of the 17-year cicadas.");
        else
            System.out.println("It’s not an emergence year for Brood II of the 17-year cicadas");

        if ((year-2013)%11==0)
            System.out.println("It’s a peak sunspot year.");
        else
            System.out.println("It’s not a peak sunspot year.");

        System.out.println("Good bye");
    }
}
