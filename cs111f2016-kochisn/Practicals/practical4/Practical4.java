/************************************
  Honor Code: This work is mine unless otherwise cited.
  Noah Kochis
  CMPSC 111
  30 September 2016
  Practical 4

  Basic Input with a dialog box
 ************************************/

import javax.swing.JOptionPane;
import java.util.Random;

public class Practical4
{
    public static void main ( String[] args )
    {
        int age = 0; // user's input - age
        int age1 = 20; // modified age

        Random random = new Random();

        // display a dialog with a message
        JOptionPane.showMessageDialog( null, "Let's get you a new identity!" );

        // prompt user to enter the first name
        String name = JOptionPane.showInputDialog(" What is your first name?");

        //create a message with the modified first name
        String newName = "Your new first name is "+ name+"ka";

        //display the message with the modified user's name
        JOptionPane.showMessageDialog(null, newName);

        // prompt user to enter the last name
        String lastName = JOptionPane.showInputDialog(" What is your last name?");

        // create message with modified last name
        String newLastName = "Your new last name is "+ lastName+"stein";

        // display the message with modified last name
        JOptionPane.showMessageDialog(null, newLastName);

        // TO DO: prompt the user to get the last name, modify the last name and display the new last name

        // prompt user to enter their fav color
        age = Integer.parseInt(JOptionPane.showInputDialog("What's your favorite number"));

        // modify the age
        age1 += random.nextInt(40);
        String newAge = "Your new age is: "+age1;

        // display a message with the new age
        JOptionPane.showMessageDialog(null, newAge);

        //prompt user for street number
        int street = Integer.parseInt(JOptionPane.showInputDialog("What's your street number"));

        // create message with new street adress
        int street1 = random.nextInt(40);
        String newStreet = "Your new street number is:  "+street1;

        // display message with new street number
        JOptionPane.showMessageDialog(null, newStreet);

        String lane = JOptionPane.showInputDialog("What street do you live on?");

        String newLane = "Your new adress is "+street1+" "+lane+"gheny lane.";

        JOptionPane.showMessageDialog(null, newLane);

        // TO DO: modify the above questions/answers to your own
        // TO DO: come up with your own (at least three) questions and answers

    } //end main
}  //end class Practical4
